/*
 * bemtools 0.1
 *
 */

// bemtools('.element', {
// 	populate: [{
// 		element: '.title',
// 		format: 'text',
// 		minLenght: 20
// 		maxLenght: 100
// 	}],
// 	toggleSingleModifier: ['collapsed', 'active'],
// 	toggleMultipleModifier: [
// 		['item', 'question'],
// 		['edit', 'view']
// 	]
// });

(function(window, document, undefined) {

	var bemtools = function(){
		var $el,
			$elContainer;		
	}

	bemtools.prototype.create = function(el, params){
		$el = $(el),
		$elContainer = $el.parent();

		if(params.repeat){
			for(var i = 1; i <= params['repeat']; i++){
				this.clone();
			}
		}

		if(params.modifiers){
			var modifiedEls = [$el];

			for(var i = 0; i < params.modifiers.length; i++){
				var group = params.modifiers[i],
					temp = [];
				
				for(var j = 0; j < group.length; j++){
					var modifier = group[j];
					temp = $.merge( this.applyModifier( modifiedEls, modifier), temp );
				}

				modifiedEls = $.merge( modifiedEls, temp )
			}
		}

		console.log( 'Total: ', modifiedEls.length );
		console.log( modifiedEls );
	}


	bemtools.prototype.applyModifier = function(elements, modifier){
		var modifiedElements = [];

		for(var i = 0; i < elements.length; i++){
			$tempEl = elements[i].clone().addClass(modifier).appendTo($elContainer);
			modifiedElements.push( $tempEl  );
		}		
		
		return modifiedElements;
	}

	bemtools.prototype.clone = function(){
		return $el.clone().appendTo( $elContainer );
	}

	bemtools.prototype.helpers = {
		toggleClass: function(params){

		},
		getNumnber: function(min, max, maxFloat){

		},
		getPhrase: function(words){
			var phrase = [];

			for(var i = 0; i <= words; i++){
				var length = Math.floor(Math.random()*8);
				phrase.push( this.getWord(length) );
			}

			return phrase.join(' 1');
		},
		/* Random word generator
		 * http://james.padolsey.com/javascript/random-word-generator/
		 */
		getWord: function(length){
		    var consonants = 'bcdfghjklmnpqrstvwxyz',
		        vowels = 'aeiou',
		        rand = function(limit) {
		            return Math.floor(Math.random()*limit);
		        },
		        i, word='', length = parseInt(length,10),
		        consonants = consonants.split(''),
		        vowels = vowels.split('');
		    for (i=0;i<length/2;i++) {
		        var randConsonant = consonants[rand(consonants.length)],
		            randVowel = vowels[rand(vowels.length)];
		        word += (i===0) ? randConsonant : randConsonant;
		        word += i*2<length-1 ? randVowel : '';
		    }
		    return word;
		}
	}
	
	window.bemtools = bemtools;

})(window, document);